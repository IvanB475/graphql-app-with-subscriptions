const { gql } = require("apollo-server");

const typeDefs = gql`
  type Query {
    hello(name: String): String
    user: User
    errorLogs: [Error!]!
  }

  type User {
    id: ID!
    username: String
    firstLetterOfUsername: String
  }

  type Error {
    field: String!
    message: String!
  }

  type RegisterResponse {
    user: User
  }

  input UserInfo {
    username: String!
    password: String!
    age: Int
  }

  type Mutation {
    register(input: UserInfo!): RegisterResponse!
    login(input: UserInfo!): String!
  }

  type Subscription {
    newUser: User!
    registeredUser: User!
    loggedinUser: String!
  }
`;

module.exports = {
  typeDefs,
};
