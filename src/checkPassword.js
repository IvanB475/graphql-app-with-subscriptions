// TODO
const bcrypt = require('bcryptjs');
const { errorDescriptor, ERROR_WRONG_PASSWORD } = require('../errors');

const checkPassword = async (password, userPassword) => {
const isEqual = await bcrypt.compare(password, userPassword);
if (!isEqual) {
    throw new Error(errorDescriptor(ERROR_WRONG_PASSWORD));
} else {
    return; 
}
};

module.exports = checkPassword;
