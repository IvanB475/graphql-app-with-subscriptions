const dotenv = require('dotenv');
dotenv.config();

const { ApolloServer, PubSub } = require("apollo-server");

// Error handling
const {
  ERRORS,
  ERROR_AUTHENTICATION_DATA_IS_MISSING,
  errorDescriptor
} = require('./errors');
const { formatError } = require('./errors/formatError');

const EVENTS = require('./src/events');
const { typeDefs } = require('./src/schema');
const bcyrpt = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');

const checkPassword = require('./src/checkPassword'); 

const users = [
  {
    id: 1,
    username: 'Pero',
    password: '$2a$12$O1TQA8DweDP8RmnU89yHSeGALT.hm6DWBEQQ/iqgsqDO4AAwMhSZa',
    admin: true,
  }
];


const resolvers = {
  Subscription: {
    newUser: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator(EVENTS.NEW_USER)
    },
    registeredUser: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator(EVENTS.REGISTRATION_SUCCESS)
    },
    loggedinUser: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator(EVENTS.LOGIN_SUCCESS)
    }
  },
  User: {
    firstLetterOfUsername: parent => {
      return parent.username ? parent.username[0] : null;
    }
    // username: parent => { return parent.username;
    // }
  },
  Query: {
    hello: (parent, { name }) => {
      return `hey ${name}`;
    },
    user: () => ({
      id: 1,
      username: "tom"
    }),
    errorLogs: () => ([
      {
        field: "username",
        message: "bad"
      },
      {
        field: "username2",
        message: "bad2"
      }
    ])
  },
  Mutation: {
    login: async (parent, args, { pubsub }) => {
      const user = users.find(user => user.username === args.input.username);

      if(user) {
        await checkPassword(args.input.password, user.password);

        pubsub.publish(EVENTS.LOGIN_SUCCESS, {
          loggedinUser: user.username
        });

        return user.username;
      } else { 
      throw new Error(errorDescriptor(ERROR_AUTHENTICATION_DATA_IS_MISSING));
      }
    },

    register: async (_, args , { pubsub }) => {
      const userPW = await bcyrpt.hash(args.input.password, 12);

      const user = {
        id: uuidv4(),
        username: args.input.username,
        password: userPW,
        admin: false
      };

      users.push(user);
      pubsub.publish(EVENTS.NEW_USER, {
        newUser: user
      });

      pubsub.publish(EVENTS.REGISTRATION_SUCCESS, {
        registeredUser: user
      });


      return {
        user
      };
    }
  }
};

const pubsub = new PubSub();

const timer = setInterval(() => {
  pubsub.publish(EVENTS.PING, {
    listenForPing: `${Date.now()}`,
  });
}, 3000);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => ({ req, res, pubsub }),
  // customFormatErrorFn: formatError,
  formatError,
});

const PORT = process.env.PORT || 4001;

server.listen(PORT)
  .then(({ url }) => console.log(`Server started at ${url}`));
